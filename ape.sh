#! /bin/sh
#
# Ape invoker.
# Preferrabley copy to be in the directory with the configurations
#

export APETOPDIR=$(realpath $(dirname $0))

if [ -d $APETOPDIR/ApeTools ]; then
    export APETOPDIR=$(dirname $APETOPDIR)
fi

exec python3 $APETOPDIR/ape-core/ape "$@"
