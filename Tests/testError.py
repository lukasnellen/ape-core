#! /usr/bin/env python3

# Configure path to find modules to test
import sys
import os.path
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

import unittest
from ApeTools import InstallError


def raiseError(args=[], cwd=None, pack=None, stage=None, log=None, cmd=None):
    raise InstallError(args, cwd, pack, stage, log, cmd)


class testError(unittest.TestCase):
    def testRaise(self):
        self.assertRaises(InstallError, raiseError)

    def testPackage(self):
        try:
            raiseError(pack="TestPackage")
        except InstallError as e:
            msg = str(e)
        self.assertEqual(msg, "!!*** FAILURE\n  *** Package: TestPackage\n!!*** FAILURE")

    def testArgs(self):
        try:
            raiseError(args=['a1', 'a2'])
        except InstallError as e:
            msg = str(e)
        self.assertEqual(msg, "!!*** FAILURE\n  *** Extra arguments:\n    a1, a2\n!!*** FAILURE")

    def testArgsPackage(self):
        try:
            raiseError(args=['a1', 'a2'], pack="TestPackage")
        except InstallError as e:
            msg = str(e)
        self.assertEqual(msg, "!!*** FAILURE\n  *** Package: TestPackage\n" +
                         "  *** Extra arguments:\n    a1, a2\n!!*** FAILURE")

if __name__ == '__main__':
    unittest.main()
