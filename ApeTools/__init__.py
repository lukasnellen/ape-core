class InstallError(Exception):
    """The generic installation error exception.

    The string representation of :exc:`InstallError` instances generates
    an error report.

    This class is the basis for all other exceptions defined in ape. It is
    caught at the top-level to generate an error report to the user before
    aborting execution.
    """
    def __init__(self, args=[], cwd=None, pack=None, stage=None, log=None,
                 cmd=[]):
        self.args = args
        self.pack = pack
        self.stage = stage
        self.cwd = cwd
        self.log = log
        try:
            self.cmd = " ".join(cmd)
        except:
            self.cmd = cmd

    def __str__(self):
        error = "!!*** FAILURE\n"
        if self.pack:
            error += f"  *** Package: {self.pack}"
        if self.stage:
            error += f"  *** Stage: {self.stage}"
        if self.cwd:
            error += f"  *** Working directory: {self.cwd}"
        if self.log:
            error += f"  *** Check the log at '{self.log}'\n"
        if self.cmd:
            error += f"  *** The command: {self.cmd}"
        if self.args:
            error += "  *** Extra arguments:\n    "
            error += ", ".join(self.args) + "\n"
        error += "!!*** FAILURE"
        return error
