__all__ = []

import glob
import os.path

# This way of doing it fails in case insensitive systems and that is why
# they don't do it by default in python.
# We can have a convention regarding case and use it.
for f in glob.glob(os.path.dirname(__file__) + '/*.py'):
    if f != __file__:
        (root, ext) = os.path.splitext(f)
        __all__.append(os.path.basename(root))

from ApePackages import *
